#!/usr/bin/env python3

import argparse as ap
import urllib3
import json

from itertools import *
from functools import wraps, lru_cache

# Functions
@lru_cache(maxsize=1000)
def currency_of(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        args = f(*args, **kwargs)
        fixerapi = "http://data.fixer.io/api/latest?access_key=ed4d3b665f8aed728799e764d8830ff1&base={}"
        fixerapi = fixerapi.format(args['basecurrency'])
        pool = urllib3.PoolManager()
        r = pool.request('GET', fixerapi)
        json_data = json.loads(r.data.decode("utf-8"))
        if not json_data['success']:
            print("API Error.")
            return
        print(json_data)
    return wrapper

@currency_of
def process(**args):
    return {**args}

parser = ap.ArgumentParser(description="Döviz aziz: En yakınındaki döviz.")

parser.add_argument('amount', type=int,
                    help="Amount of your current currency")

parser.add_argument('basecurrency', type=str,
                    help="Your base currency to convert.")

parser.add_argument('tocurrency', type=str,
                    help="To the currency")

args = parser.parse_args()

print(args)
process(**vars(args))
